//
// Created by Isaias Perez
// Goal: To find the shortest exit, uses BFS to search for paths that may lead to an exit, if no exit is found
// program will return -1.
// Maze Escape
// ----------------------------* "~" Indicates an exit
// Input Example:              | "-" Indicates a valid path
// 2                           | "X" Indicates a path block
// 5 6                         | "S" Indicates the starting point
// ~~~~~~                      | Must be capital letters ("S", "X")
// ~XXXX~                      |
// ~XS--~                      | Compile: gcc MazeEscape.c
// ~-XX-~                      | Run: ./a.out
// ~~~~~~                      |
// 6 7                         |
// ~~~~~~~                     |
// ~XX---~                     |
// ~XX-XX~                     |
// ~XS-XX~                     |
// ~XXXXX~                     |
// ~~~~~~~                     |
// ----------------------------*


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// CONSTANTS //
const char EXIT = '~';
const char START = 'S';
const char BLOCK = 'X';
const char OPEN = '-';
const int NO_VISIT = -1;

// COORDINATE STRUCT //
typedef struct point {
    int x;
    int y;
} point;

// QUEE STRUCT //
typedef struct quee {
    point* coordinates;
    int frontIndex;
    int elementCount;
    int size;
    
} quee;

// FUNCTION SIGNATURES //
void setUp(char** board, int** path, int x , int y, quee* myQ);
void buildQuee(quee* myQ);
void enquee(quee* myQ, int x, int y);
void dequee(quee* myQ, int* x, int* y);
int runBreadthFirstAlg(quee* myQ, char** board, int** path, int row, int col);
void cleanUp(quee myQ, char** board, int** path, int x);


int main(void) {
    
    int numCase, x, y; // xy  = row col
    int distance = 0;
    scanf("%d", &numCase);
    
    // For every case
    for (int i = 0; i < numCase; i++) {
        scanf("%d%d", &x, &y);
        
        // Memory for input board array and path array
        char** board = NULL; int** path = NULL;
        board = malloc(sizeof(char*) * x);
        path = malloc(sizeof(int*) * x);
        for (int i = 0; i < x; i++) {
            board[i] = (char*)malloc(sizeof(char) * y);
            path[i] = (int*)malloc(sizeof(int) * y);
        }
        
        // Prepare quee and board structure
        quee myQ; buildQuee(&myQ);
        setUp(board, path, x, y, &myQ);
        
        // Print shortest path to EXIT print -1 if none
        distance = runBreadthFirstAlg(&myQ, board, path, x, y);
        printf("Shortest Exit: %d\n", distance);

    }
    
    return 0;
}

// READ BOARD, RESET PATH AND INITIALIZE QUEE //
void setUp(char** board, int** path, int x , int y, quee* myQ) {
    
    // Read in board and set path array
    for (int i = 0; i < x; i++) {
        for (int j = 0; j < y; j++) {
            path[i][j] = NO_VISIT;
            int temp = getchar(); char temp2 = 0;
            while (isspace(temp)) {
                temp = getchar();
                temp2 = temp;
            }
            
            if (temp == 'S' || temp == 'X') {
                board[i][j] = temp2;
            }
            
            board[i][j] = temp;
            
            // Enquee start point
            if (board[i][j] == START) {
                path[i][j] = 0;
                
                enquee(myQ, i, j);
            }
        }
    }
}

// SETUP DEFAULTED QUEE //
void buildQuee(quee* myQ) {
    
    // min size = 3 x 3
    if (myQ) {
        myQ->size = 8;
        myQ->coordinates = (point*)malloc(sizeof(point) * myQ->size);
        myQ->elementCount = 0;
        myQ->frontIndex = 0;
    }
}

// ADD ELEMENT TO THE QUEE, SIZE ACCORDINGLY //
void enquee(quee* myQ, int x, int y) {
    
    if (myQ) {
        // Double size of quee if it's full
        if (myQ->elementCount == myQ->size) {
            
            // Coordinates index shifted-looped by (Front + Count) % Size
            point* shiftedCoordinates = NULL; int newSize = myQ->size * 2;
            shiftedCoordinates = (point*)malloc(sizeof(point) * newSize);
            for (int i = 0; i < myQ->elementCount; i++) {
                shiftedCoordinates[i] = myQ->coordinates[(myQ->frontIndex + myQ->elementCount) % myQ->size];
            }
            myQ->frontIndex = 0; free(myQ->coordinates);
            myQ->size = newSize; myQ->coordinates = NULL;
            myQ->coordinates = shiftedCoordinates;
        }
        
        // Adding point to the quee, coordinates index shifted-looped by (Front + Count) % Size
        point pointEnqued;
        pointEnqued.x = x; pointEnqued.y = y;
        myQ->coordinates[(myQ->frontIndex + myQ->elementCount) % myQ->size] = pointEnqued;
        
        // Update quee count
        (myQ->elementCount)++;
    }
}

// REMOVE ELEMENT FROM THE FRONT //
void dequee(quee* myQ, int* x, int* y) {
    
    if (x) {
        *x = myQ->coordinates[myQ->frontIndex].x;
    }
    if (y) {
        *y = myQ->coordinates[myQ->frontIndex].y;
    }
    
    // Check if we are at the end of the quee, adjust front if so
    if (++myQ->frontIndex >= myQ->size) {
        myQ->frontIndex -= myQ->size;
    }
    // Update quee count
    (myQ->elementCount)--;
}


// RUN BFS ALGO //
int runBreadthFirstAlg(quee* myQ, char** board, int** path, int row, int col) {
    int minDistance = -1;
    if (myQ) {
        int x = 0; int y = 0;
        while (myQ->elementCount) {
            
            // Dequee front of quee
            dequee(myQ, &x, &y);
            
            // Checking if we are one step from the exit
            if (board[x - 1][y] == EXIT || board[x + 1][y] == EXIT || board[x][y -1] == EXIT || board[x][y + 1] == EXIT) {
                minDistance = path[x][y] + 1;
                
                // Stop looking and return distance
                return minDistance;
            }
            
            // look right
            if (board[x][y+1] != BLOCK && path[x][y+1] == NO_VISIT) {
                path[x][y+1] = path[x][y] + 1;
                enquee(myQ, x, y + 1);
            }
            // look left
            if (board[x][y-1] != BLOCK && path[x][y-1] == NO_VISIT) {
                path[x][y-1] = path[x][y] + 1;
                enquee(myQ, x, y - 1);
            }
            // look down
            if (board[x+1][y] != BLOCK && path[x+1][y] == NO_VISIT) {
                path[x+1][y] = path[x][y] + 1;
                enquee(myQ, x + 1, y);
            }
            // look up
            if (board[x-1][y] != BLOCK && path[x-1][y] == NO_VISIT) {
                path[x-1][y] = path[x][y] + 1;
                enquee(myQ, x - 1, y);
            }
        }
    }
    cleanUp(*myQ, board, path, row);
    return minDistance;
}

// FREE ALLOCATED MEMORY //
void cleanUp(quee myQ, char** board, int** path, int x) {
    for (int i = 0; i < x; i++) {
        free(board[i]); free(path[i]);
    }
    free(board); free(path); free(myQ.coordinates);
    board = NULL; path = NULL;
}


