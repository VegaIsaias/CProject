//
//  Created by Isaias Perez Vega
//  -------------------------------------------------------------------------
//  Program finds out if for a number of vacations, if it is possible to visit all rides given some are blocked, locations of rides follow. If it is poosble it will output the time it would take.
//
//  Input example: First integer indicates number of vacations
//                 Following line indicates how many rides in total and how many blocked
//                 Ride coordinates follow for the next (rides + blocked) lines
//  1
//  4 0
//  0 2
//  2 2
//  2 4
//  4 4
//  5 4
//  --------------------------------------------------------------------------
//  Compile: gcc vacation.c
//  Run: ./a.out



#include <stdio.h>
#include <stdlib.h>

#define MAXRIDES 11


// Coordinate struct
struct xyCoordinate {
    double x;
    double y;
};


// Func Signatures
double getPath(int* perm, int* used, int k, int n, struct xyCoordinate* ridePoints, int** blockedPath);
double getDistane(struct xyCoordinate* pointA, struct xyCoordinate* pointB);
double defineDistance(int* perm, struct xyCoordinate* ridePoints, int n);
double sqrRoot(double square);
void destroyArray(int** array, int size);
int* makeArray(int* array, int size);



int main(void) {
    
    // Get number of vacationss
    int numVacations;   scanf("%d", &numVacations);
    
    // For every vacation
    for (int x = 1; x <= numVacations; x++) {
        int numRide, numBlocked;
        scanf("%d%d", &numRide, &numBlocked);
        
        
        // Space for rides
        struct xyCoordinate* ridePoints;
        ridePoints = (struct xyCoordinate*)malloc(sizeof(struct xyCoordinate*) * MAXRIDES);
        
        // For every ride, read location
        for (int y = 0; y < numRide; y++) {
            int rideX, rideY;
            scanf("%d%d", &rideX, &rideY);
            ridePoints[y].x = (double)rideX;
            ridePoints[y].y = (double)rideY;
        }
        
        
        // Space for blocked paths
        int** blockedPath = NULL;
        blockedPath = (int**)malloc(sizeof(int*) * MAXRIDES);
        for (int x = 0; x < MAXRIDES; x++) {
            blockedPath[x] = makeArray(blockedPath[x], MAXRIDES);
        }
        
        
        // Reset Array
        for (int x = 0; x < MAXRIDES; x++) {
            for (int y = 0; y < MAXRIDES; y++) {
                blockedPath[x][y] = 0;
            }
        }
        
        
        // Adjacent locations are blocked
        for (int x = 0; x < numBlocked; x++) {
            int block1, block2;
            scanf("%d%d", &block1, &block2);
            blockedPath[block1-1][block2-1] = 1;
            blockedPath[block2-1][block1-1] = 1;
        }
        
        int* perm;
        perm = makeArray(perm, MAXRIDES);
        int* used;
        used = makeArray(used, MAXRIDES);
        
        // No visits to rides yet
        for (int x = 0; x < numRide; x++) {
            used[x] = 0;
            perm[x] = -1;
        }
        
        double answer = getPath(perm, used, 0, numRide, ridePoints, blockedPath);
        
        printf("\n");
        printf("Vacation #%d:\n", x);
        
        if (answer < 999999) {
            printf("Jimmy can finish all of the rides in %.3lf seconds. \n\n", answer);
        } else {
            printf("Jimmy should plan this vacation a different day.\n\n");
        }
    
        // Clean Up
        //destroyArray(blockedPath, MAXRIDES);
        free(used); free(perm);
        free(ridePoints); ridePoints = NULL;
    }
    
    return 0;
}


// Finds the length of the shortest path that only visits a ride once //
double getPath(int* perm, int* used, int k, int n, struct xyCoordinate* ridePoints, int** blockedPath) {
    // Base case
    if (k == n) {
        return defineDistance(perm, ridePoints, n) + (double)k* 120.0;
    } else {
        double shortest = 1000000.0;
        
        // If not blocked find shortest distance from each ride
        for (int x = 0; x < n; x++) {
            if (!used[x]) {
                if (k == 0 || blockedPath[perm[k-1]][x] == 0) {
                    perm[k] = x;
                    used[x] = 1;
                    
                    // Check plan B
                    double planB = getPath(perm, used, k + 1, n, ridePoints, blockedPath);
                    
                    // Check if its the best path
                    if (planB < shortest) {
                        shortest = planB;
                    }
                    // Update marker
                    used[x] = 0;
                }
            }
        }
        return shortest;
    }
}


// Get the fistance from origin to point //
double defineDistance(int* perm, struct xyCoordinate* ridePoints, int n) {

    struct xyCoordinate origin;
    origin.x = 0, origin.y = 0;
    
    // Get initial distance
    double distance = getDistane(&origin, &ridePoints[perm[0]]);
    
    // Add the rest of the edges
    for (int x = 1; x < n; x++) {
        distance += getDistane(&ridePoints[perm[x - 1]], &ridePoints[perm[x]]);
    }
    
    return distance;
}
 

// Get distance between two points //
double getDistane(struct xyCoordinate* pointA, struct xyCoordinate* pointB) {
    return sqrRoot(((pointA->x - pointB->x)*(pointA->x - pointB->x)) + ((pointA->y - pointB->y)*(pointA->y - pointB->y)));
}


// Get square root of number //
double sqrRoot(double square) {
    double root = square / 3;
    if (square <= 0)
        return 0;
    for (int x = 0; x < 32; x++)
        root = (root + square / root) / 2;
    return root;
}

// Return array of ints //
int* makeArray(int* array, int size) {
    array = NULL;
    array = (int*)malloc(sizeof(int) * size);
    return array;
}

// Clean up //
void destroyArray(int** array, int size) {
    for (int x = 0; x < size; x++) {
        free(array[x]);
    }
    free(array); array = NULL;
}

